
var windowWidth, windowHeight, collapsedMinHeight, selectListHeight, readMoreMobileState;
var scrollPosition = null;
var mySwiper;
var map_x, map_y, mapSize;
var text_mas = [];

$(document).ready(function() {
	
	
	/* main slider */
	if($('#main-slider')[0]){
		
		$('#main-slider').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed: 1000,
			slidesToShow: 1,
			fade: true,
			appendDots: '.slider-pagination',
			autoplay: true,
			autoplaySpeed: 6000
		});
		
	}
	//--
	
	
	/* clients slider */
	if($('#clients-slider')[0]){
		sliderInit();
	}
	//--
	
	
	/* client slider popups */
	(function () {
		var items = $('.b-clients-slider .slide'),
			popups = $('.popup-client');
			var slide_cb = function (e) {
				var slide = $(this),
					pos = slide.offset(),
					slide_width = slide.width(),
					slide_height = slide.height(),
					popup = popups.filter('.popup-' + ((slide.attr('href').split('#')[1] || '').match(/id-(\d+)/) || [])[1]);
					if (!popup.is(':visible')) {
						popup.css({ display: 'block', opacity: '0' });
						var popup_height = popup.height();
						$('.popup-client').css('width', slide_width + slide_width/100*22);
						popup.css({
							top: pos.top - ((popup_height - slide_height) / 2) + 'px',
							left: pos.left - ((slide_width/100*22)/2) + 'px'
						}).animate({ opacity: 1 }, 300);
						popups.not(popup).fadeOut(300);
					} else {
						popups.fadeOut(300);
					}
				return false;
			};
			var doc_cb = function (e) {
				popups.fadeOut(300);
			};
			items.click(slide_cb);
			$(document).click(doc_cb);
			popups.click(doc_cb);
	})();
	//--

	
	/* custom select */
	$('body').addClass('hasJS');
	$("select.custom").each(function() {
		
		var sb = new SelectBox({
			selectbox: $(this),
			customScrollbar: true,
			height: selectListHeight,
			scrollOptions: {
				autoReinitialise: true,
				showArrows: true
			},
			changeCallback: function(val) {   
		  	$(this)[0].selectbox.change();
			}  
		});
		
	});
	//--
	
	
	/* mobile menu */
	$(document).click( function(event){
		if( $(event.target).closest('header').length ) 
		return;
		$('body').removeClass('menu-toggle');
		event.stopPropagation();
	});
	$('.mobile-menu-toggle').click(function(){
		
		if (!$('body').hasClass('menu-toggle')) {
			scrollPosition = $(document).scrollTop();
			$('body').toggleClass('menu-toggle').css('top', '-'+scrollPosition+'px');
		} else {
			$('body').removeClass('menu-toggle');
			$(document).scrollTop(scrollPosition);
		}
		
	});
	//--
	
	
	/* search form */
	$(document).click( function(event){
		if( $(event.target).closest('.b-form-search').length ) 
		return;
		if(windowWidth < 999)
			$('.b-search__input').fadeOut(500);
		event.stopPropagation();
	});
	$('.b-form-search__icon').click(function(){
		$(this).parent().find('.b-search__input').fadeToggle(500);
	});
	//--
	
	
	/* mobile content */
	$('.b-headline h2, .b-headline h3').click(function(){
		if(windowWidth < 920){
			$(this).parent().toggleClass('open-content');
			$(this).parent().next('.mobile-content').slideToggle(500);
			if($('#clients-slider')[0]){
				mySwiper.update(true);
			}
		}
	});
	$('.close-content').click(function(){
		$(this).parent().removeClass('open-content');
		$(this).parent().next('.mobile-content').slideUp(500);
	});
	//--
	
	
	/* mobile text cut */
	$('.b-read-more').click(function(){
		if(windowWidth < 725){
			$(this).fadeOut(0);
			$('.b-hidden-text').css('display', 'inline');
			$('.b-mobile-ellipsis').css('display', 'none');
			readMoreMobileState = 1;
		}
		return false;
	});
	$('.b-read-more_hide').click(function(){
		if(windowWidth < 725){
			$('.b-hidden-text').css('display', 'none');
			$('.b-read-more').fadeIn(300);
			$('.b-mobile-ellipsis').css('display', 'inline-block');
			readMoreMobileState = null;
		}
		return false;
	});
	//--
	
	
	/* submenu */
	$('.b-menu li').hover(function () {
		if(windowWidth > 920 && !device.mobile() && !device.tablet()){
			 clearTimeout($.data(this,'timer'));
			 $('ul',this).stop(true,true).slideDown(150);
		}
	}, function () {
		if(windowWidth > 920 && !device.mobile() && !device.tablet()){
			$.data(this,'timer', setTimeout($.proxy(function() {
				$('ul',this).stop(true,true).slideUp(150);
			}, this), 100));
		}
	});
	
	$('.b-menu li').click(function () {
		if($('ul',this).is(':visible')){
			$('ul',this).stop(true,true).slideUp(150);
		}
		else{
			$('ul',this).stop(true,true).slideDown(150);
		}
	});
	//--
	
	
	setTimeout(function(){
		$('.b-select').css('opacity', '1');
	}, 400);
	
	
	
	/* login modal */
	$('.b-btn__office-2').on('click', function(e) {

			e.preventDefault();
			$('#login_modal').bPopup({
				opacity: 0.6,
				transition: 'fadeIn',
				speed: 500,
			});

	});
	//--
	
	
	/* password modal */
	$('#forgot-pass-link').bind('click', function(e) {

			e.preventDefault();
			$('#password_modal').bPopup({
				onOpen: function() { // close #login_modal 
					var bPopup = $('#login_modal').bPopup();
					bPopup.close();
				},
				opacity: 0.6,
				transition: 'fadeIn',
				speed: 500,
			});

	});
	//--
		
	
	/* registration modal */
	$('#register_btn').bind('click', function(e) {

			e.preventDefault();
			$('#registration_modal').bPopup({
				onOpen: function() { // close #login_modal 
					var bPopup = $('#login_modal').bPopup();
					bPopup.close();
				},
				opacity: 0.6,
				transition: 'fadeIn',
				speed: 500,
			});

	});
	//--
	
	
	/* count modal */
	$('.b-btn__calculate').bind('click', function(e) {

			e.preventDefault();
			$('#count_modal').bPopup({
				opacity: 0.6,
				transition: 'fadeIn',
				speed: 500,
			});

	});
	//--
	
	
	/* mobile search modal */
	$('.b-btn__search').bind('click', function(e) {

			e.preventDefault();
			$('#search_modal').bPopup({
				opacity: 0.6,
				transition: 'fadeIn',
				speed: 500,
			});

	});
	//--
	
	
	/* list in 2 columns */
	if($('#list-col')[0])
		$('#list-col').columnize({ columns: 2 });
	//--
	
	
	/* info accordion */
	$('.b-info__hd-box').click(function(){
		$(this).parent().toggleClass('b-info__open');
		$(this).next('.b-info__text').slideToggle(500);
		$(this).find('.b-info__details').text($(this).find('.b-info__details').text() == 'Скрыть' ? 'Подробнее' : 'Скрыть');
		if(!$(this).parent().hasClass('b-info__open'))
			$(this).parent().find('.b-info__close-mobile').fadeOut(0);
		else
			$(this).parent().find('.b-info__close-mobile').fadeIn(0);
	});
	$('.b-info__close-mobile').click(function(){
		$(this).fadeOut(0);
		$(this).parents('.b-info__text').slideUp(500);
		$(this).parents('.b-info').removeClass('b-info__open');
	});
	//--
	
	
	/* contacts accordion with clock */
	clock();
	var allAccordions = $('.b-contacts__content');
  var allAccordionItems = $('.b-contacts__item');
  $('.b-contacts__item').click(function(){
    if($(this).hasClass('contacts_open')){
      $(this).removeClass('contacts_open');
      $(this).find('.b-contacts__content').slideUp(500);
			if($(this).find('.b-branch-open').length > 0){
				$(this).find('.b-branch-open').text('открыть');
			}
    }
    else{
			allAccordions.slideUp(500);
			allAccordionItems.removeClass('contacts_open');
			$(this).addClass('contacts_open');
			$(this).find('.b-contacts__content').slideDown(500);
			if($(this).find('.b-branch-open').length > 0){
				$(this).find('.b-branch-open').text('свернуть');
			}
    }
  });
	//--
	
	
	/* examples */
	$('.b-examples__hd').click(function(){
		$(this).next('.b-examples__content').slideToggle(500);
		
		$(this).find('span').text($(this).text() == 'Свернуть примеры' ? 'Посмотреть примеры' : 'Свернуть примеры');
		
		if(!$(this).hasClass('example_open')){
			$(this).addClass('example_open');
			
			/* examples slider */
			if($('.b-examples-slider').length > 0){
				$('.b-examples-slider').slick({
					dots: false,
					arrows: false,
					infinite: true,
					speed: 800,
					slidesToShow: 5,
					swipe: false,
					responsive: [
					{
						breakpoint: 1300,
						settings: {
							slidesToShow: 4,
							arrows: true
						}
					},
					{
						breakpoint: 1180,
						settings: {
							slidesToShow: 3,
							arrows: true
						}
					},
					{
						breakpoint: 740,
						settings: {
							slidesToShow: 2,
							arrows: true
						}
					},
					{
						breakpoint: 450,
						settings: {
							slidesToShow: 1,
							arrows: true
						}
					}
					]
				});
			}
			
		}
		else{
			$(this).removeClass('example_open');
			if($('.b-examples-slider').length > 0){
				setTimeout(function(){
					$('.b-examples-slider').slick('unslick');
				}, 400);
			}
		}
		//--
	});
	//--
	
	
	/* description slider */
	if($('.description-cards-slider').length > 0){
		$('.description-cards-slider').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 800,
			slidesToShow: 3,
			swipe:false,
			responsive: [
			{
				breakpoint: 1180,
				settings: {
					slidesToShow: 2,
					arrows: true
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 1,
					arrows: true
				}
			}
			]
		});
	}
	//--
	
	
	/* articles */
	$('.article-details').click(function(){
		if(windowWidth > 740){
			$(this).parents('.b-articles__item').toggleClass('article-open').find('.hidden-text').slideToggle(500);
			$(this).text($(this).text() == 'Свернуть' ? 'Подробнее' : 'Свернуть');
		}
		else{
			$(this).parents('.b-articles__item').toggleClass('article-open').find('.b-articles__text').slideToggle(500);
			$(this).parents('.b-articles__item').find('.b-mobile .article-details_mobile').text('Читать');
		}
	});
	$('.article-details_mobile').click(function(){
		$(this).parents('.b-articles__item').toggleClass('article-open').find('.b-articles__text').slideToggle(500);
		$(this).text($(this).text() == 'Свернуть' ? 'Читать' : 'Свернуть');
		$(this).parents('.b-articles__item').find('.article-details').text('Свернуть');
	});
	//--
	
	
	/* input file */
	$('#input-file').change(function(){
		$('.inputFileVal, .remove-file').css('opacity', '1');
		if(windowWidth < 450){
			$('.type_file').css('margin-top', '30px');
		}
	});
	$('.remove-file').click(function(){
		$('#fileName').val('');
		$('.inputFileVal, .remove-file').css('opacity', '0');
		if(windowWidth < 450){
			$('.type_file').css('margin-top', '0px');
		}
	});
	//--
	
	
	/* google maps */
	if($('#contacts-map')[0]){
		
		google.maps.event.addDomListener(window, 'load', init);
		google.maps.event.addDomListener(window, "resize", function init(){});
		
		var markerIcon = {
			url: 'img/marker.png',
		}
				
		function init() {
				var mapOptions = {
						zoom: 14,
						center: new google.maps.LatLng(map_y, map_x),
						disableDefaultUI: true
				};
				var mapElement = document.getElementById('map');
				var map = new google.maps.Map(mapElement, mapOptions);
				
				var infobox = new InfoBox({
					content: document.getElementById("infobox"),
					disableAutoPan: false,
					alignBottom: true,
					pixelOffset: new google.maps.Size(7, -30),
					zIndex: null,
					boxStyle: {
						opacity: 1,
						zIndex: 999
					},
					closeBoxMargin: "0 0 -60px -60px",
					closeBoxURL: "img/close.png",
					infoBoxClearance: new google.maps.Size(1, 1)
				});
				
				var marker = new google.maps.Marker({
						position: new google.maps.LatLng(43.3099075, 76.9557147),
						map: map,
						icon: markerIcon
				});	
				marker.addListener('click', function() {
					infobox.open(map, this);
				});

		}
			
	}
	
	
	if($('#map-stock')[0]){
		
		google.maps.event.addDomListener(window, 'load', init);
		google.maps.event.addDomListener(window, "resize", function init(){});
		
		var markerIcon = {
			url: 'img/marker2.png',
		}
				
		function init() {
				var mapOptions = {
						zoom: 3,
						center: new google.maps.LatLng(35.2858545, 113.293806),
						disableDefaultUI: true
				};
				var mapElement = document.getElementById('map');
				
				var map = new google.maps.Map(mapElement, mapOptions);
				
				var infobox = new InfoBox({
					content: document.getElementById("infobox"),
					disableAutoPan: false,
					alignBottom: true,
					pixelOffset: new google.maps.Size(7, -30),
					zIndex: null,
					boxStyle: {
						opacity: 1,
						zIndex: 999,
					},
					closeBoxMargin: "0px",
					closeBoxURL: "",
					infoBoxClearance: new google.maps.Size(1, 1)
				});
				
				var marker = new google.maps.Marker({
						position: new google.maps.LatLng(23.1358545, 113.293806),
						map: map,
						icon: markerIcon
				});	
				marker.addListener('mouseover', function() {
					infobox.open(map, this);
				});
				marker.addListener('mouseout', function() {
					infobox.close(map, this);
				});
				
				var marker2 = new google.maps.Marker({
						position: new google.maps.LatLng(41.0608157, 117.514625),
						map: map,
						icon: markerIcon
				});	
				marker2.addListener('mouseover', function() {
					infobox.open(map, this);
				});
				marker2.addListener('mouseout', function() {
					infobox.close(map, this);
				});
				
				var marker3 = new google.maps.Marker({
						position: new google.maps.LatLng(49.636393, 117.897797),
						map: map,
						icon: markerIcon
				});	
				marker3.addListener('mouseover', function() {
					infobox.open(map, this);
				});
				marker3.addListener('mouseout', function() {
					infobox.close(map, this);
				});
				
		}
		
	}
	
	
	if($('#branches-map')[0]){
		
		google.maps.event.addDomListener(window, 'load', init);
		google.maps.event.addDomListener(window, "resize", function init(){});	
		
		var markerIcon = {
			url: 'img/marker.png',
		}
				
		function init() {
				var mapOptions = {
						zoom: mapSize,
						center: new google.maps.LatLng(map_y, map_x),
						disableDefaultUI: true
				};
				var mapElement = document.getElementById('map');
				var map = new google.maps.Map(mapElement, mapOptions);
				
				var infobox = new InfoBox({
					content: document.getElementById("infobox"),
					disableAutoPan: false,
					alignBottom: true,
					pixelOffset: new google.maps.Size(7, -30),
					zIndex: null,
					boxStyle: {
						opacity: 1,
						zIndex: 999,
					},
					closeBoxMargin: "0 0 -60px -60px",
					closeBoxURL: "img/close.png",
					infoBoxClearance: new google.maps.Size(0, 0)
				});
				
				var marker = new google.maps.Marker({
						position: new google.maps.LatLng(44.229395, 80.0668144),
						map: map,
						icon: markerIcon
				});	
				marker.addListener('click', function() {
					infobox.open(map, this);
				});
				
				var marker2 = new google.maps.Marker({
						position: new google.maps.LatLng(41.0608157, 117.514625),
						map: map,
						icon: markerIcon
				});	
				marker2.addListener('click', function() {
					infobox.open(map, this);
				});
				
				var marker3 = new google.maps.Marker({
						position: new google.maps.LatLng(56.009657, 37.9456611),
						map: map,
						icon: markerIcon
				});	
				marker3.addListener('click', function() {
					infobox.open(map, this);
				});
				
				var marker4 = new google.maps.Marker({
						position: new google.maps.LatLng(42.9506032, 74.7188189),
						map: map,
						icon: markerIcon
				});	
				marker4.addListener('click', function() {
					infobox.open(map, this);
				});

		}
	}
	//--
	
	
	/* forms validation */
	
	/* login */
	if($('#login-form')[0]){
		$('#login-form').validate({
			
			errorClass: "has-error",
			validClass: "has-success",
			
			errorPlacement: function(error, element) {},
			
			highlight: function(element, errorClass, validClass) {
				
				$(element).parent().addClass(errorClass).removeClass(validClass);
				
			},
			
			unhighlight: function(element, errorClass, validClass) {
				
				$(element).parent().removeClass(errorClass).addClass(validClass);
				
			},
			
			rules: {
				
				login: {
					required: true
				},
				
				password: {
					required: true
				}
				
			},
			
			invalidHandler: function(event, validator) {
				
				errors = validator.numberOfInvalids();
				
				if (errors) {
					var message = 'Вы ввели неверные логин или пароль!';
					$(".b-error-text").html(message);
					$(".b-error-text").show();
				} else {
					$(".b-error-text").hide();
				}
				
			}
			
			
		});
	}
	/* //-- login */
	
	/* registration */
	if($('#register-form')[0]){
		$('#register-form').validate({
			
			errorClass: "has-error",
			validClass: "has-success",
			
			errorPlacement: function(error, element) {
				error.appendTo(element.parent().find('.b-error-message'))
			},
			
			onkeyup: function(element, errorClass, validClass) {
				
				if($(element).hasClass('b-password')){
					$(element).parent().addClass(errorClass).removeClass(validClass);
					
					if($("input[name*='register_pass']").val() == $("input[name*='register_pass_again']").val()){
						$(element).trigger('blur');
					}
				}
				
			},
			
			highlight: function(element, errorClass, validClass) {
				
				$(element).parent().addClass(errorClass).removeClass(validClass);
				
			},
			
			unhighlight: function(element, errorClass, validClass) {
				
				$(element).parent().removeClass(errorClass).addClass(validClass);
				
			},
			
			rules: {
				
				register_name: {
					required: true
				},
				
				register_phone: {
					required: false,
					pattern: "\\+7\\s\\(([0-9]{3})\\)\\s([0-9]{3})\\s([0-9]{2})\\s([0-9]{2})"
				},
				
				register_mail: {
					required: true,
					email:true
				},
				
				register_pass: {
					required: true,
					equalTo: "input[name*='register_pass_again']"
				},
				
				register_pass_again: {
					required: true,
					equalTo: "input[name*='register_pass']"
				},
				
				register_terms: {
					required: true
				}
				
			},
			
			messages: {
				
				register_name: {
					required: 'поле не заполнено'
				},
				
				register_phone: {
					pattern: 'некорректный номер'
				},
				
				register_mail: {
					required: 'поле не заполнено',
					email: 'e-mail введен неверно'
				},
				
				register_pass: {
					required: 'поле не заполнено',
					equalTo: ''
				},
				
				register_pass_again: {
					required: 'поле не заполнено',
					equalTo: 'пароли не совпадают'
				},
				
				register_terms: {
					required: ''
				}
				
			},
			
			submitHandler: function() { 
			
				$('#registration_info_modal').bPopup({
					onOpen: function() { // close #registration_modal 
						var bPopup = $('#registration_modal').bPopup();
						bPopup.close();
					},
					opacity: 0.6,
					transition: 'fadeIn',
					speed: 500,
				});
			
			}
			
		});
	}
	/* //-- registration */
	
	
	/* count */
	if($('#count-form')[0]){
		$('#count-form').validate({
			
			errorClass: "has-error",
			validClass: "has-success",
			
			errorPlacement: function(error, element) {
				error.appendTo(element.parent().find('.b-error-message'))
			},
			
			highlight: function(element, errorClass, validClass) {
				
				$(element).parent().addClass(errorClass).removeClass(validClass);
				
			},
			
			unhighlight: function(element, errorClass, validClass) {
				
				$(element).parent().removeClass(errorClass).addClass(validClass);
				
			},
			
			rules: {
				
				count_name: {
					required: true
				},
				
				count_phone: {
					pattern: "\\+7\\s\\(([0-9]{3})\\)\\s([0-9]{3})\\s([0-9]{2})\\s([0-9]{2})"
				},
				
				count_mail: {
					required: true,
					email:true
				},
							
				count_terms: {
					required: true
				}
				
			},
			
			messages: {
				
				count_name: {
					required: 'поле не заполнено'
				},
				
				count_phone: {
					pattern: 'некорректный номер'
				},
				
				count_mail: {
					required: 'поле не заполнено',
					email: 'e-mail введен неверно'
				},
				
				count_terms: {
					required: ''
				}
				
			},
			
			submitHandler: function() { 
			
				$('#count_info_modal').bPopup({
					onOpen: function() { // close #count_modal 
						var bPopup = $('#count_modal').bPopup();
						bPopup.close();
					},
					opacity: 0.6,
					transition: 'fadeIn',
					speed: 500,
				});
			
			}
			
		});
	}
	/* //-- count */
	
	
	/* renew password */
	if($('#renew-password')[0]){
		$('#renew-password').validate({
			
			errorClass: "has-error",
			validClass: "has-success",
			
			errorPlacement: function(error, element) {
				error.appendTo(element.parent().find('.b-error-message'))
			},
			
			highlight: function(element, errorClass, validClass) {
				
				$(element).parent().addClass(errorClass).removeClass(validClass);
				
			},
			
			unhighlight: function(element, errorClass, validClass) {
				
				$(element).parent().removeClass(errorClass).addClass(validClass);
				
			},
			
			rules: {
				
				renew_mail: {
					required: true,
					email:true
				}
				
			},
			
			messages: {
	
				renew_mail: {
					required: 'поле не заполнено',
					email: 'e-mail введен неверно'
				}
				
			},
			
			submitHandler: function() { 
			
				$('#password_info_modal').bPopup({
					onOpen: function() { // close #password_modal 
						var bPopup = $('#password_modal').bPopup();
						bPopup.close();
					},
					opacity: 0.6,
					transition: 'fadeIn',
					speed: 500,
				});
	
			}
			
		});
	}
	/* //-- renew password */
	
	//-- end validation
	
	
	/* search results - remove cutting */
	if($('.b-results__text').length > 0){
		$('.b-results__text').each(function(i){
			text_mas[i] = $('.b-results__text:eq('+i+')').html();
		});
	}
	//--
	
	
	/* phone mask */
	if($('.b-input_phone').length > 0) {
		
		$('.b-input_phone').each(function(){
			$(this).inputmask("+7 (999) 999 99 99"); 
		});
		
	}
	//--
	
	
	
	$(window).resize(onResize);
	onResize();	
	
});



/* resize */
function onResize(){
	
	windowWidth = $(window).width();
	windowHeight = $(window).height();
	
	if (!$('body').hasClass('menu-toggle')) {
		scrollPosition = $(document).scrollTop();
	} else {
		$(document).scrollTop(scrollPosition);
	}
	
	if(windowWidth >= 625)
		collapsedMinHeight = 86
	else
		collapsedMinHeight = 106
	
	
	if(windowWidth >= 725){
		$('.b-hidden-text').css('display', 'inline');
		$('.b-mobile-ellipsis').css('display', 'none');
	}
	else if(!readMoreMobileState){
		$('.b-hidden-text').css('display', 'none');
		$('.b-mobile-ellipsis').css('display', 'inline');
	}
	else{
		$('.b-hidden-text').css('display', 'inline');
	}
	
	
	if($('#clients-slider')[0]){
		mySwiper.update(true);
	}
	
	
	if(windowWidth >= 920){
		$('.mobile-content').each(function(){
			if(!$(this).is(':visible')){
				$(this).css('display', 'block').addClass('temp');
			}
		});
		
		selectListHeight = 117;
	}
	else{
		$('.mobile-content').each(function(){
			if($(this).hasClass('temp')){
				$(this).css('display', 'none').removeClass('temp');
			}
		});
		
		selectListHeight = 129;
	}
	
	
	if($('.popup-client').is(':visible')){
		$('.popup-client').css('display', 'none');
	}
	
	
	if($('.b-results__text').length > 0){
		if(windowWidth < 740){
			$('.b-results__text').each(function(i){
				cutLongString($('.b-results__text:eq('+i+')'), 117, false);
			});
		}
		else{
			$('.b-results__text').each(function(i){
				$('.b-results__text:eq('+i+')').html(text_mas[i]);
			});
		}
	}
	
	if(windowWidth < 640){
		map_y = 43.31399;
		map_x = 76.9611129;
		mapSize = 2;
	}
	else{
		map_y = 43.31399;
		map_x = 76.9727119;
		mapSize = 3;
	}
	
}
//--


/* scroll top */
$(function() {
	$.fn.scrollToTop = function() {
	
		$(this).hide().removeAttr("href");
		if ($(window).scrollTop() != "0") {
				$(this).fadeIn("slow")
		}
		var scrollDiv = $(this);
		var footerPos = $('.b-footer').offset().top;
		var windowHeight = $(window).height();
		
		$(window).scroll(function() {
			if ($(window).scrollTop() == "0") {
					$(scrollDiv).fadeOut("fast")
			} else{
					$(scrollDiv).fadeIn("fast")
			}
			
			if ($(window).scrollTop() + windowHeight >= footerPos+$('.b-footer').height()-20){
				$(scrollDiv).css('bottom', '312px');
			}
			else{
				$(scrollDiv).css('bottom', '20px');
			}
				
		});
		$(this).click(function() {
				$("html, body").animate({
						scrollTop: 0
				}, "slow")
		})
	}
});
$(function() {
	if($('.go-top').length > 0){
		$('.go-top').scrollToTop();
	}
});
//--


/* fix menu */
$(function() {
	var footerPos = $('.b-footer').offset().top;
	var fixMenuFlag = 0;
	var $window = $(window);
	$window.on('load scroll', function() {
		if(windowWidth > 920 && $('.b-footer').height() > 1){
			var top = $window.scrollTop();
			var height = $window.height();
			if (top + height >= footerPos && fixMenuFlag == 0 && $(window).scrollTop() > 0) {
					fixMenu();
					fixMenuFlag = 1;
			}
			else if (top + height < footerPos && fixMenuFlag == 1){
				fixAllHeader();
				fixMenuFlag = 0;
			}
			if($(window).scrollTop() == 0){
				fixAllHeader();
				fixMenuFlag = 0;
			}
		}
	});
	function fixMenu() {
		$('.b-header').animate({
			top: '-93px'
		}, 500);
	}
	function fixAllHeader() {
		$('.b-header').animate({
			top: '0px'
		}, 500);
	}
});
//--


/* clients slider init */
function sliderInit(){
	
	mySwiper = new Swiper('#clients-slider', {
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		slidesPerView: 6,
		spaceBetween: 36,
		grabCursor: true,
		breakpoints: {
			1320: {
				slidesPerView: 5
			},
			1270: {
				slidesPerView: 4
			},
			1180: {
				slidesPerView: 3
			},
			820: {
				slidesPerView: 2
			},
			750: {
				slidesPerView: 1
			}
		}
	});
	
}
//--


/* tabs */
(function($) {
  $(function() {
 
    $('ul.b-tabs').delegate('li:not(.current-tab)', 'click', function() {
      $(this).addClass('current-tab').siblings().removeClass('current-tab')
        .parents('div.tabs-content').eq(0).find('>div.b-box').hide().eq($(this).index()).fadeIn(500).show();
			 $(this).parents('div.tabs-content').eq(0).find('>div.b-box_2').hide().eq($(this).index()).fadeIn(500).show();
				
				/* description slider */
				if($('.description-cards-slider').length > 0){
					$('.description-cards-slider').slick('unslick');
					$('.description-cards-slider').slick({
						dots: false,
						arrows: false,
						infinite: true,
						speed: 800,
						slidesToShow: 3,
						swipe:false,
						responsive: [
						{
							breakpoint: 1180,
							settings: {
								slidesToShow: 2,
								arrows: true
							}
						},
						{
							breakpoint: 700,
							settings: {
								slidesToShow: 1,
								arrows: true
							}
						}
						]
					});
				}
				//--
			

			return false;	
    });
 
  })
})(jQuery)
//--


/* text cutting */
function cutLongString(element, count_lit, light){
	
	// text
	text = element.html();
	// text length
	var all_len = text.length;
	// new text
	var new_text;
	
	 
	if (all_len > count_lit && windowWidth < 740){
		// cutting
		new_text = text.substr(0, (count_lit - 3)) + '....';
		// replace text
		element.html(new_text);
	}
	
}
//--


/* current time */
function clock() {
	var d = new Date();
	var hours = d.getHours();
	var minutes = d.getMinutes();
	var seconds = d.getSeconds();
	
	if (hours <= 9) hours = "0" + hours;
	if (minutes <= 9) minutes = "0" + minutes;
	if (seconds <= 9) seconds = "0" + seconds;
	
	date_time = hours + ":" + minutes + "<span>:" + seconds + "</span>";
	
	$('.current_time').html(date_time);
	
	setTimeout("clock()", 1000);
}
//--